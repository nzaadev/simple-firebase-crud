package com.imanancin.firebasecrud;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import java.io.IOException;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    UserAdapter userAdapter;
    RecyclerView rvUser;
    ProgressBar progressBar;
    FirebaseManager firebaseManager;
    View user_form;
    ImageView imageView;
    SearchView searchView;
    ArrayList<User> tmpUserList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        user_form = getLayoutInflater().inflate(R.layout.add_user, null);
        imageView = user_form.findViewById(R.id.ivImage);

//        if(!LoginActivity.isLogin) {
//            startActivity(new Intent(this, LoginActivity.class));
//            finish();
//        } else {
            initUi();
            addDialog();
            initFirebase();
//        }

    }

    private void initFirebase() {
        firebaseManager = new FirebaseManager();
        firebaseManager.getDataFromFirebase().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                progressBar.setVisibility(View.GONE);
                ArrayList<User> userList = new ArrayList<>();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {

                    User user = dataSnapshot.getValue(User.class);
                    user.setId(dataSnapshot.getKey());
                    userList.add(user);
                }
                tmpUserList.clear();
                tmpUserList.addAll(userList);
                userAdapter.submitList(userList);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    void initUi () {
        progressBar = findViewById(R.id.progressBar);
        rvUser = findViewById(R.id.rvUser);
        rvUser.setLayoutManager(new GridLayoutManager(this, 1));
        userAdapter = new UserAdapter();
        userAdapter.setListener(user -> editDialog(user).show());
        rvUser.setAdapter(userAdapter);
        searchView = findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ArrayList<User> tmp = new ArrayList<>();
                if(newText.length() > 0) {
                    // filter
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        tmpUserList.forEach(user -> {
                            if(user.getName().contains(newText)) {
                                tmp.add(user);
                            }
                        });
                    }
                    userAdapter.submitList(tmp);
                } else {
                    userAdapter.submitList(tmpUserList);
                }
                return true;
            }
        });
    }

    public static final int PICK_IMAGE = 1;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);


        if (requestCode == PICK_IMAGE) {
            progressDialog.setTitle("Upload gambar...");
            progressDialog.show();
            Uri imageUri = data.getData();
            @SuppressLint("Recycle") Cursor returnCursor = getContentResolver().query(imageUri, null, null, null, null);
            int nameIndex = 0;
            if (returnCursor != null) {
                nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
            }
            if (returnCursor != null) {
                returnCursor.moveToFirst();
            }
            String filename = returnCursor.getString(nameIndex);

            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                String imageRef = System.currentTimeMillis() + "." + MimeTypeMap.getFileExtensionFromUrl(filename);
                final StorageReference storageReference = firebaseManager.uploadFileFirebase(imageRef);
                UploadTask uploadTask = storageReference.putFile(imageUri);
                uploadTask.continueWithTask(task -> {

                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return storageReference.getDownloadUrl();
                }).addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Toast.makeText(MainActivity.this, "Gagal upload gambar", Toast.LENGTH_SHORT).show();
                    } else {
                        imageView.setContentDescription(task.getResult().toString());
                        imageView.setTag(imageRef);
                        Toast.makeText(MainActivity.this, "Sukses upload", Toast.LENGTH_SHORT).show();
                    }
                    progressDialog.dismiss();
                });


            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            imageView.setImageBitmap(bitmap);
        }
    }
    void pickImage() {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

        startActivityForResult(chooserIntent, PICK_IMAGE);
    }
    AlertDialog.Builder addDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Add User");
        imageView.setOnClickListener(v -> pickImage());
        alertDialog.setView(user_form);
        ((EditText) user_form.findViewById(R.id.edtName)).setText("");
        ((EditText) user_form.findViewById(R.id.edtAddress)).setText("");
        ((ImageView) user_form.findViewById(R.id.ivImage)).setImageResource(R.drawable.baseline_add_24);
        alertDialog.setCancelable(false);
        alertDialog.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());
        alertDialog.setPositiveButton("Save", (dialog, which) -> {

            String name  = ((EditText) user_form.findViewById(R.id.edtName)).getText().toString();
            String address = ((EditText) user_form.findViewById(R.id.edtAddress)).getText().toString();
            CharSequence imageUrl = imageView.getContentDescription();
            Object imageViewTag = imageView.getTag();
            String imageViewTagS = "";
            if(imageUrl == null) {
                imageUrl = "";
            }
            if(imageViewTag != null) {
               imageViewTagS = imageViewTag.toString();
            }

            firebaseManager
                    .addDataToFirebase(new User(name, address, imageUrl.toString(), imageViewTagS))
                    .addOnSuccessListener(unused ->
                            Toast.makeText(MainActivity.this, "Berhasil di simpan", Toast.LENGTH_SHORT).show());
        });

        return alertDialog;
    }
    AlertDialog.Builder editDialog(User user) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Update User");
        EditText edtName = user_form.findViewById(R.id.edtName);
        EditText edtAddress = user_form.findViewById(R.id.edtAddress);
        ImageView imageView1 = user_form.findViewById(R.id.ivImage);
        if(user.getImageUrl().contains("http")) {
            Glide.with(this)
                    .load(user.getImageUrl())
                    .into(imageView1);
        }
        edtName.setText(user.getName());
        edtAddress.setText(user.getAddress());
        alertDialog.setView(user_form);
        alertDialog.setCancelable(false);
        alertDialog.setNeutralButton("Cancel", (dialog, which) -> {
            dialog.dismiss();
        });
        alertDialog.setPositiveButton("Update", (dialog, which) -> {
            String name  = edtName.getText().toString();
            String address = edtAddress.getText().toString();
            String imageUrl = "";
            String imageRef = "";
            if(imageView.getContentDescription() != null) {
                imageUrl = imageView.getContentDescription().toString();
            }
            if(imageView.getTag() != null) {
                imageRef = imageView.getTag().toString();
            }
            firebaseManager
                    .updateData(user.getId(), new User(name, address, imageUrl, imageRef))
                    .addOnSuccessListener(unused ->
                            Toast.makeText(MainActivity.this, "Berhasil di update", Toast.LENGTH_SHORT).show());
        });
        alertDialog.setNegativeButton("Delete", (dialog, which) -> {
            firebaseManager.delete(user.getId()).addOnSuccessListener(unused -> {
                Toast.makeText(MainActivity.this, "Berhasil di hapus", Toast.LENGTH_SHORT).show();
            });

            if(imageView.getTag() != null) {
                firebaseManager.deleteFileFirebase(imageView.getTag().toString());
            }

        });
        return alertDialog;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.add_user) {
            addDialog().show();
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        if(firebaseManager!=null) {
            firebaseManager.cleanup();
        }
        super.onDestroy();
    }
}