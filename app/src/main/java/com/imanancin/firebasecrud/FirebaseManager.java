package com.imanancin.firebasecrud;

/**
 * Created by : iman
 * Date : 01/06/24
 */

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class FirebaseManager {
    private FirebaseDatabase firebaseDatabase;
    private FirebaseStorage firebaseStorage;
    private DatabaseReference databaseReference;
    private StorageReference storageReference;

    public FirebaseManager() {
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("users");
        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference("images");
    }


    public Task<Void> addDataToFirebase(Object data) {
        return databaseReference.push().setValue(data);
    }


    public DatabaseReference getDataFromFirebase() {
        return databaseReference;
    }

    public Task<Void> delete(String node) {
        return databaseReference.child(node).removeValue();
    }

    public Task<Void> updateData(String node, Object data) {
        return databaseReference.child(node).setValue(data);
    }

    public StorageReference uploadFileFirebase(String filename) {
        return storageReference.child(filename);
    }

    public void deleteFileFirebase(String filename) {
        storageReference.child(filename).delete();
    }

    public void cleanup() {
        firebaseDatabase.goOffline();
    }
}
