package com.imanancin.firebasecrud;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

/**
 * Created by : iman
 * Date : 01/06/24
 */
public class UserAdapter extends ListAdapter<User, UserAdapter.ViewHolder> {

    protected UserAdapter() {
        super(new DiffUtil.ItemCallback<User>() {
            @Override
            public boolean areItemsTheSame(@NonNull User oldItem, @NonNull User newItem) {
                return oldItem == newItem;
            }

            @Override
            public boolean areContentsTheSame(@NonNull User oldItem, @NonNull User newItem) {
                return oldItem.getId().equals(newItem.getId());
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // TODO: Layout item change here
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(getCurrentList().get(position), listener);
    }

    Listener listener;
    void setListener(Listener listener) {
        this.listener = listener;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        // Note binding here
         TextView tvName = itemView.findViewById(R.id.tvName);
         TextView tvAddress = itemView.findViewById(R.id.tvAddress);
         ImageView profileImg = itemView.findViewById(R.id.profileImg);
        public ViewHolder(View view) {
            super(view);
        }

        public void bind(User model, Listener listener) {
             tvName.setText(model.getName());
             tvAddress.setText(model.getAddress());
            if(model.getImageUrl().startsWith("http")) {
                Glide.with(itemView.getContext())
                        .load(model.getImageUrl())
                        .into(profileImg);
            }
             itemView.setOnClickListener(v -> {
                 listener.onClick(model);
             });
        }
    }

    public interface Listener {
        void onClick(User user);
    }
}