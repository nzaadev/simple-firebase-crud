package com.imanancin.firebasecrud;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.io.Serializable;

/**
 * Created by : iman
 * Date : 01/06/24
 */
public class User  {

    public  String id;

    public String name;
    public String address;
    public String imageUrl;
    public String imageRef;

    public User() {}

    public User(String name, String address, String imageUrl, String imageRef) {
        this.name = name;
        this.address = address;
        this.imageUrl = imageUrl;
        this.imageRef = imageRef;
    }

    public User(String id, String name, String address, String imageUrl, String imageRef) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.imageUrl = imageUrl;
        this.imageRef = imageRef;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageRef() {
        return imageRef;
    }

    public void setImageRef(String imageRef) {
        this.imageRef = imageRef;
    }
}
